import React from 'react';
import { Row, Col, Avatar, Popover } from 'antd';

const Footer = () => {
  return (
    <div style={{ height: 150, backgroundColor: '#DF6245', marginTop: 20 }}>
      <Row type='flex' justify="center">
        <Col>
          <Popover content={<a href='https://www.facebook.com/somsudee.rotrungruengkit'>"it's my Facebook 👤"</a>} title='Face me!'>
            <Avatar size={70} style={{ backgroundColor: '#4088DA', margin: '40px 50px 40px 50px' }} icon='facebook'/>
          </Popover>
        </Col>
        <Col>
          <Popover content={<a href='https://www.instagram.com/cucumber2912/'>"it's my Instagram 📸"</a>} title='Insta Me!'>
            <Avatar size={70} style={{ backgroundColor: '#FABF62', margin: '40px 50px 40px 50px' }} icon='instagram' />
          </Popover>
        </Col>
        <Col>
          <Popover content={'souleater_oyeah@live.com'} title='my Email 📫'>
            <Avatar size={70} hoverable style={{ backgroundColor: '#25CE9E', margin: '40px 50px 40px 50px' }} icon='mail' />
          </Popover>
        </Col>
      </Row>
    </div>
  );
}

export default Footer;