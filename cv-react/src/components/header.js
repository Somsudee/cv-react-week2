import React from 'react';
import { Nav } from 'react-bootstrap';
import { Row, Col, } from 'antd';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/Header.css';

class Header extends React.Component {

    constructor() {
        super();

        this.state = {
            pages: './aboutMe'
        }
    }

    changePage = (page) => {
        this.setState({
            pages: page
        })
    }

    render() {
        return (
            <div className="header">
                <div className="container-fluid">
                    <Row type='flex' justify='center'>
                        <Col>
                            <img style={{
                                width: 250,
                                height: 250,
                                margin: '6% auto',
                                backgroundColor: '#ECE9D6',
                                border: '5px solid #ECE9D6',
                                borderRadius: '50%'
                            }} src="./profile.jpg" />
                        </Col>
                        <Col>
                            <Row style={{
                                fontSize: 55,
                                fontFamily: 'Geneva',
                                textAlign: 'left',
                                verticalAlign: 'middle',
                                color: '#ECE9D6',
                                margin: '8% 0px 8% 3%',
                                textTransform: 'uppercase',
                            }}>
                                <Col style={{ fontSize: 75 }}>
                                    Somsudee
                                </Col>
                                <Col>
                                    Rotrungruengkit
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
                <div className="row">
                    <div className="col">
                        <Nav justify='center' variant="tabs">
                            <Nav.Item>
                                <Nav.Link href="./aboutMe" >About me</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link href="./gpa" >GPA</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link href="./anime2" >Anime</Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </div>
                </div>
            </div>
        )
    }
}

export default Header;