import React from 'react';
import { carouselCss, image, slide, } from '../../css/historyPic';
import { Carousel, Row, Col, Card, } from 'antd';

const { Meta } = Card;

const HistoryPic = () => {

  return (
    <div>
      <Carousel autoplay style={carouselCss}>
        <div>
          <Row type='flex' justify='center'>
            <Col>
              <Card
                style={slide}
                cover={<img style={image} src="/his.jpg" />}
              >
                <Meta 
                  title="Trekking Doi Suthep" 
                  description=" 
                    &emsp;&emsp;&emsp;This is picture of all my friends,
                    we take this photo because of making profile
                    for learning througth activities for our point
                    and at that time we prepare everyone to ready
                    for a walk." 
                />
              </Card>
            </Col>
          </Row>
        </div>
        <div>
          <Row type='flex' justify='center'>
            <Col>
            <Card
                style={slide}
                cover={<img style={image} src="/openhouse62.jpg" />}
              >
                <Meta 
                  title="Open House 62" 
                  description=" 
                    &emsp;&emsp;&emsp;I'm only third year who came to
                    watch out second and first year presentation, others
                    of my friend are still sleeping even though we already
                    made appointment." 
                />
              </Card>
            </Col>
          </Row>
        </div>
        <div>
          <Row type='flex' justify='center'>
            <Col>
              <Card
                  style={slide}
                  cover={<img style={image} src="/china.jpg" />}
                >
                  <Meta 
                    style={{color: 'white',}}
                    title="China Trip" 
                    description=" 
                      &emsp;&emsp;&emsp;We went to China for two weeks
                      , it seem long but it not that long days, we still
                      have many place to go but my friends not ready to walk
                      so decide to went only near place that we can walk because
                      bus driver drive so scarely." 
                  />
                </Card>
              </Col>
          </Row>
        </div>
      </Carousel>
    </div>
  );
}
export default HistoryPic;