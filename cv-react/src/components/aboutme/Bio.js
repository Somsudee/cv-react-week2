import React from 'react';
import { Row, Col, Avatar } from 'antd';

const Bio = () => {
  return (
    <div style={{
      backgroundColor: '#ECE9D6',
      padding: '20px',
    }}>
      <Row type='flex' justify='center'>
        <Col>
          <div
            style={{
              height: 300,
              width: '500px',
              border: 3,
              borderRadius: 10,
              margin: '10px 30px 10px 10px',
              textAlign: 'center',
              fontSize: 18,

              backgroundColor: '#FFFFFF',
            }}
          >
            <h1 style={{ paddingTop: '20px' }}><Avatar size={40} style={{ backgroundColor: '#87d068', margin: '5px' }} icon="user" />Bio</h1>
            <div style={{ padding: '5px',
              textAlign: 'left' }}>
              <p>
                &emsp;My name is Somsudee Rotrungurengkit, my nickname is Tangkwa but
                others alway called me cucumber or TK (stand for Tangkwa).
              </p>
              <p><Avatar style={{backgroundColor:'#FFBA4B'}} icon='calendar'/> 29 December 1998</p>
              <p><Avatar style={{backgroundColor:'#E75252'}} icon="star" /> Sports: Volleyball, Tabletennis </p>
            </div>
          </div>
        </Col>
        <Col>
          <div
            style={{
              height: 300,
              width: '500px',
              border: 3,
              borderRadius: 10,
              margin: '10px 30px 10px 10px',
              fontSize: 20,

              backgroundColor: '#FFFFFF',
            }}
          >
            <h1 style={{ textAlign: 'center', margin: '0px 0px 25px 0px', paddingTop: '20px' }}>
              <Avatar
                size={40}
                style={{
                  backgroundColor: '#f56a00',
                  textAlign: 'center',
                  margin: '5px',
                }}
                icon="rocket"
              />
              Preference
            </h1>
            <ul>
              <li>
                Pet my turtles.
              </li>
              <li>
                Baking Bakery.
              </li>
              <li>
                Singing.
              </li>
              <li>
                Build the models of important building.
              </li>
              <li>
                Paint a art or do D.I.Y.
              </li>
              <li>
                Fold an origami.
              </li>
            </ul>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default Bio;