import React from 'react';
import { Progress, Row, Col, Tag } from 'antd';

const mySkill = () => {

    const Skills = (percent, skill) => {
        return (
            <div style={{ fontSize: 20, }}>
                <p>
                    {percent}%
                </p>
                <b style={{ fontSize: 30 }}>
                    {skill}
                </b>
            </div>
        );
    }

    return (
        <div>
            <div
                style={{
                    width: 1050,
                    backgroundColor: '#FFFFFF',
                    borderRadius: 10,
                    textAlign: 'center',
                    margin: '0 auto',
                    padding: '10px',
                }}>
                <h1>My Skills</h1>
                <Row type='flex' justify='center'>
                    <Col>
                        <Progress style={{ margin: 15 }} width={150} strokeColor={{ direction: '#F2C033' }} type="dashboard" percent={80} format={percent => Skills(percent, 'HTML')} />
                        <Progress style={{ margin: 15 }} width={150} strokeColor={{ direction: '#E25B52' }} type="dashboard" percent={70} format={percent => Skills(percent, 'CSS')} />
                        <Progress style={{ margin: 15 }} width={150} strokeColor={{ direction: '#7071A9' }} type="dashboard" percent={70} format={percent => Skills(percent, 'JS')} />
                        <Progress style={{ margin: 15 }} width={150} strokeColor={{ direction: '#0799D1' }} type="dashboard" percent={60} format={percent => Skills(percent, 'JAVA')} />
                        <Progress style={{ margin: 15 }} width={150} strokeColor={{ direction: '#00A6A2' }} type="dashboard" percent={50} format={percent => Skills(percent, 'REACT')} />
                    </Col>
                </Row>
                <hr />
                <Row type='flex' justify='center' style={{ fontSize: 24 }}>
                    <Col style={{ marginRight: 10 }}>
                        <p>Language:</p>
                    </Col>
                    <Col>
                        <Tag color='blue'>English </Tag>
                    </Col>
                    <Col>
                        <Tag color='cyan'>Japanese *Basic </Tag>
                    </Col>
                    <Col>
                        <Tag color='red'>Chinese *Basic </Tag>
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default mySkill;