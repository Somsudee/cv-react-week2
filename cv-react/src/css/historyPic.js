export const carouselCss = {
  textAlign: 'center',
  lineHeight: '160px',
  background: '#ECE9D6',
  overflow: 'hidden'
}

export const image = {
  height: 350,
  width: '100%',
  borderRadius: '10px 10px 0px 0px '
}

export const slide = {
  height: 475,
  width: 625,
  backgroundColor: '#FFFFFF',
  borderRadius: 10,
}

export const textContainer = {
  color: '#FFFFFF',
}