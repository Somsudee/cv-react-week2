import React, { useEffect, useState } from 'react';
import { Avatar, Icon, Spin, Input, List, Row, Col, Select, IconText } from 'antd';
import Footer from '../components/Footer';

const { Search } = Input;
const { Option } = Select;

const Anime2 = () => {
    const [animes, setAnimes] = useState([]);
    const [query, setQuery] = useState('');
    const [isLoading, setLoading] = useState(true);
    const [type, setType] = useState('All');
    const [types, setTypes] = useState([])

    useEffect(() => {
        fetchAnime();
    }, [])

    const fetchAnime = () => {
        fetch('https://api.jikan.moe/v3/search/anime?q=one%20punch%20man')
            .then(response => response.json())
            .then(data => {
                setAnimes(data["results"])
                let filterItems = types;
                data.results.map((anime) => {
                    if (!filterItems.find(x => x === anime.type)) {
                        filterItems.push(anime.type);
                    }
                });
                setTypes(filterItems);
                setTimeout(() => {
                    setLoading(false)
                }, 500)

            })
            .catch(error => console.log(error));
    }

    const fetchAnimeQuery = () => {
        fetch('https://api.jikan.moe/v3/search/anime?q=' + query)
            .then(response => response.json())
            .then(data => {
                setAnimes(data["results"])
                let filterItems = types;
                data.results.map((anime) => {
                    if (!filterItems.find(x => x === anime.type)) {
                        filterItems.push(anime.type);
                    }
                });
                setTypes(filterItems);
                setTimeout(() => {
                    setLoading(false)
                }, 500)
            })
            .catch(error => console.log(error));
    }

    const IconText = ({ type, text }) => (
        <span>
            <Icon type={type} style={{ marginRight: 8 }} />
            {text}
        </span>
    );

    const handleChangeQuery = (event) => {
        const query = event.target.value;
        setQuery(query);
        fetchAnimeQuery();
    }

    const onChange = (value) => {
        setType(value);
    }

    return (
        <div style={{ minHeight: '100vh', backgroundColor: '#ECE9D6' }}>
            <Row type='flex' justify='center' style={{ padding: 10 }}>
                <Col style={{ width: 700, backgroundColor: '#DF6245', borderRadius: 10, paddingTop: '15px', }}>
                    <Row type='flex' justify='center'>
                        <Col>
                            <Search
                                placeholder="Search anime..."
                                onChange={handleChangeQuery}
                                style={{ width: 500, height: 40 }}
                            />
                        </Col>
                        <Col>
                            <Select defaultValue={'All'}
                                style={{ width: 120, height: 50, margin: '5px 0px 0px 10px' }}
                                onChange={onChange}
                                filterOption={(input, option) =>
                                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                            >
                                <Option value='All'>All</Option>
                                {types.map((data, index) => {
                                    return (
                                        <Option value={index}>{data}</Option>
                                    )
                                })}
                            </Select>
                        </Col>
                    </Row>
                </Col>
            </Row>

            <div style={{ backgroundColor: 'white', width: 900, margin: '0 auto', borderRadius: 10 }}>
                {
                    isLoading == true ?
                        <Row type="flex" justify="center" style={{ marginTop: "50px" }}>
                            <Col >
                                <Spin size="large" />
                            </Col>
                        </Row>
                        :
                        <div>
                            <List
                                itemLayout="vertical"
                                size="large"
                                style={{ marginLeft: 20 }}
                                pagination={{
                                    onChange: page => {
                                        console.log(page);
                                    },
                                    pageSize: 3,
                                }}
                                dataSource={
                                    type === 'All' ?
                                        animes
                                        :
                                        animes.filter(m => m.type === types[type])
                                }
                                load={isLoading}
                                type={type}
                                footer={
                                    <div>

                                    </div>
                                }
                                renderItem={item => (
                                    <List.Item
                                        key={item.title}
                                        actions={[
                                            <IconText type="star-o" text={item.type} key="list-vertical-star-o" />,
                                            <IconText type="like-o" text={item.score} key="list-vertical-like-o" />,
                                            <IconText type="message" text={item.members} key="list-vertical-message" />,
                                        ]}
                                        extra={
                                            <img
                                                width={400}
                                                height={250}
                                                alt="logo"
                                                src={item.image_url}
                                            />
                                        }
                                    >
                                        <List.Item.Meta
                                            avatar={<Avatar size="large" icon="user" />}
                                            title={<a href={item.href}>{item.title}</a>}
                                            description={item.synopsis}
                                        />
                                    </List.Item>
                                )}
                            />
                        </div>
                }
            </div>
            <Footer />
        </div>
    )
}

export default Anime2;
