import React, { useEffect, useState } from 'react';
import { Button, Form, Row, Col } from 'react-bootstrap';
import { Avatar, Card, Icon, Popover, Empty } from 'antd';

const Anime = () => {
    const [animes, setAnimes] = useState([]);
    const [searchText, setSearchText] = useState('xxx');
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        fetchAnimes();
    }, [])

    const { Meta } = Card;

    const fetchAnimes = () => {
        fetch('https://api.jikan.moe/v3/search/anime?q=one%20punch%20man&limit=8')
            .then(response => response.json())
            .then(data => {
                setAnimes(data.results)
                console.log(data.results)

            })
            .catch(error => console.log(error));
    }

    var check = '';

    const fetchAnime = (name) => {
        var web = "https://api.jikan.moe/v3/search/anime?q=" + name.anime;
        fetch(web)
            .then(response => response.json())
            .then(data => {
                setAnimes(data.results)
                console.log(data.results)

            })
            .catch(error => console.log(error));
    }

    var anime = '';

    const search = (event) => {
        anime = event.target.value;
    }

    return (
        <div>
            <div className="container" style={{ marginTop: '15px' }}>
                <Form.Row>
                    <div className="col-2" >
                        <Button variant='warning' style={{ height: '45px', width: '150px', fontSize: '20px' }} onClick={() => fetchAnime({ anime })}><b>Search</b></Button>
                    </div>
                    <Form.Group className="col-9" controlId="exampleForm.ControlSelect1">
                        <Form.Control style={{ height: '45px', width: '1000px' }} type="text" placeholder="Search Anime here" onChange={search} />
                    </Form.Group>
                </Form.Row>
            </div>

            {/* ant design */}
            <div className="row">
                {
                    animes.map((item) => {
                        return (
                            <div className="col-3 d-flex justify-content-center">
                                <Card
                                    style={{ width: 350 }}
                                    cover={
                                        <img
                                            variant="top"
                                            style={{ height: '330px' }}
                                            alt="example"
                                            src={item.image_url}
                                        />
                                    }
                                    actions={[
                                        <Popover placement="top" title='Link' content={item.url} trigger="click">
                                            <Icon type="link" />
                                        </Popover>
                                    ]}
                                >
                                    <Meta
                                        style={{ height: '150px' }}
                                        avatar={<Avatar size="large" style={{ backgroundColor: '#e25737' }} icon="user" />}
                                        title={item.title}
                                        description={item.synopsis}
                                    />
                                </Card>
                                <br />
                            </div>
                        )
                    })
                }
            </div>
        </div >
    )
}

export default Anime;
