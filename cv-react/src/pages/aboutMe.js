import React from 'react';
import Bio from '../components/aboutme/Bio';
import '../css/aboutMe.css';
import Skill from '../components/aboutme/Skill';
import Footer from '../components/Footer';
import HistoryPic from '../components/aboutme/HistoryPic';

const aboutMe = () => {

    return (
        <div style={{
            backgroundColor: '#ECE9D6',
            minHeight: '100vh',
        }}>
            <h1
                style={{
                    fontSize: 60,
                    color: '#DF6245',
                    padding: 5,
                    textAlign: 'center',
                }}>
                About Me
            </h1>
            <HistoryPic />
            <Bio />
            <Skill />
            <Footer />
        </div>
    );
}

export default aboutMe;