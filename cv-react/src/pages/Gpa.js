import React, { useEffect, useState } from 'react';
import '../css/gpa.css';
import { Table, Form } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from '../components/Footer';
import { Row, Col } from 'antd';

const Gpa = () => {

    const semesterData = [
        {
            year: 1,
            firstSem: [
                {
                    subjectCode: '001101', subjectName: 'Fundamental English 1', credit: 3.00, grade: 'A'
                },
                {
                    subjectCode: '206113', subjectName: 'Cal For Software Engineering', credit: 3.00, grade: 'C'
                },
                {
                    subjectCode: '751100', subjectName: 'Economics For Everyday Life', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '951100', subjectName: 'Modern Life And Animation', credit: 3.00, grade: 'B+'
                },
                {
                    subjectCode: '953103', subjectName: 'Programming Logical Thinking', credit: 2.00, grade: 'B'
                },
                {
                    subjectCode: '953211', subjectName: 'Computer Organization', credit: 3.00, grade: 'B+'
                }
            ],
            secSem: [
                {
                    subjectCode: '001102', subjectName: 'Fundamental English 2', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '011251', subjectName: 'Logic', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '953102', subjectName: 'ADT & Problem Solving', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '953104', subjectName: 'Web UI Design & Develop', credit: 3.00, grade: 'B+'
                },
                {
                    subjectCode: '953202', subjectName: 'Introduction to SE', credit: 2.00, grade: 'B'
                },
                {
                    subjectCode: '953231', subjectName: 'Object Oriented Programming', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '955100', subjectName: 'Learning Through Activities 1', credit: 1.00, grade: 'A'
                }
            ],
            gpas: {
                firstSemRecord: {
                    enrolledCredit: 17, receivedCredit: 17, gpa: 3.18, gpax: 3.18
                }, secSemRecord: {
                    enrolledCredit: 18, receivedCredit: 18, gpa: 3.11, gpax: 3.14
                }
            }
        },
        {
            year: 2,
            firstSem: [
                {
                    subjectCode: '001201', subjectName: 'Crit Read And Effect Write', credit: 3.00, grade: 'C'
                },
                {
                    subjectCode: '206281', subjectName: 'Discrete Mathematics', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '953212', subjectName: 'DB Sys and DB Sys Design', credit: 3.00, grade: 'B+'
                },
                {
                    subjectCode: '953103', subjectName: 'Programming Methodology', credit: 3.00, grade: 'C'
                },
                {
                    subjectCode: '953261', subjectName: 'Interactive Web Development', credit: 3.00, grade: 'B+'
                },
                {
                    subjectCode: '953361', subjectName: 'Comp Network and Protocol', credit: 3.00, grade: 'D'
                }
            ],
            secSem: [
                {
                    subjectCode: '001225', subjectName: 'English in Science and Tech Cont', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '057122', subjectName: 'Swimming For Life And Exercise', credit: 3.00, grade: 'A'
                },
                {
                    subjectCode: '206255', subjectName: 'Math For Software Tech', credit: 3.00, grade: 'C+'
                },
                {
                    subjectCode: '953201', subjectName: 'Algo Design and Analysis', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '953214', subjectName: 'OS and Prog Lang Principles', credit: 3.00, grade: 'B+'
                },
                {
                    subjectCode: '953232', subjectName: 'OO Analysis and Design', credit: 3.00, grade: 'c+'
                },
                {
                    subjectCode: '953234', subjectName: 'Advanced Software Design', credit: 3.00, grade: 'A'
                }
            ],
            gpas: {
                firstSemRecord: {
                    enrolledCredit: 17, receivedCredit: 17, gpa: 2.44, gpax: 2.91
                }, secSemRecord: {
                    enrolledCredit: 18, receivedCredit: 18, gpa: 3.18, gpax: 2.99
                }
            }
        },
        {
            year: 3,
            firstSem: [
                {
                    subjectCode: '208263', subjectName: 'Elementary Statistics', credit: 3.00, grade: 'D'
                },
                {
                    subjectCode: '259109', subjectName: 'Telecom In Thailand', credit: 3.00, grade: 'C+'
                },
                {
                    subjectCode: '269111', subjectName: 'Commu tech in A Changing World', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '888102', subjectName: 'Big Data For Analysis', credit: 3.00, grade: 'A'
                },
                {
                    subjectCode: '953321', subjectName: 'Software Req Analysis', credit: 3.00, grade: 'B'
                },
                {
                    subjectCode: '953322', subjectName: 'Software Design and Arch', credit: 3.00, grade: 'C+'
                },
                {
                    subjectCode: '953331', subjectName: 'Compo-Based Software Dev', credit: 3.00, grade: 'B'
                }
            ],
            gpas: {
                firstSemRecord: {
                    enrolledCredit: 17, receivedCredit: 17, gpa: 2.71, gpax: 2.92
                }
            }
        }
    ]

    const [selectedYear, setSelectedYear] = useState([]);

    useEffect(() => {
        setSelectedYear(semesterData);
    }, [])

    const selectYear = (event) => {
        var year = event.target.value;
        var tem = [];
        if (year == 1) {
            tem.push(semesterData[0])
        } else if (year == 2) {
            tem.push(semesterData[1])
        } else if (year == 3) {
            tem.push(semesterData[2])
        } else {
            tem = semesterData
        }
        setSelectedYear(tem);
    }

    return (
        <div className="GPATable" style={{ paddingTop: '10px' }}>
            <div className="container" style={{ width: 700, backgroundColor: '#DF6245', borderRadius: 10, padding: 3 }}>
                <Form.Group controlId="exampleForm.ControlSelect1">
                    <Row>
                        <Col span={6}>
                            <Form.Label style={{ margin: '20px 0px 0px 20px', fontSize: 20, color: '#FFFFFF' }}><b>Select year: </b></Form.Label>
                        </Col>
                        <Col span={18}>
                            <Form.Control as="select" onChange={selectYear} style={{ width: 400, marginTop: '3%' }}>
                                <option value="all">All year</option>
                                <option value='1'>1st year</option>
                                <option value='2'>2nd year</option>
                                <option value='3'>3rd year</option>
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>
            </div>

            <div className="container">
                {
                    selectedYear.map((year) => {
                        return (
                            <div>
                                <h1 className="titleYear">{year.year} year </h1>
                                <Table striped hover>
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Course ID</th>
                                            <th>Course title</th>
                                            <th>Credit</th>
                                            <th>Grade</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            year.firstSem.map((subject, index) => {
                                                return (
                                                    <tr>
                                                        <td>{index + 1}</td>
                                                        <td>{subject.subjectCode}</td>
                                                        <td>{subject.subjectName}</td>
                                                        <td>{subject.credit}</td>
                                                        <td>{subject.grade}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                </Table>

                                <Table>
                                    <thead>
                                        <tr>
                                            <th>credits</th>
                                            <th>enrolled credits</th>
                                            <th>GPA</th>
                                            <th>GPAX</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            <tr>
                                                <td>{year.gpas.firstSemRecord.enrolledCredit}</td>
                                                <td>{year.gpas.firstSemRecord.receivedCredit}</td>
                                                <td>{year.gpas.firstSemRecord.gpa}</td>
                                                <td>{year.gpas.firstSemRecord.gpax}</td>
                                            </tr>
                                        }
                                    </tbody>
                                </Table>
                                {
                                    year.secSem ?
                                        <div>
                                            <br />
                                            <Table striped hover>
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Course ID</th>
                                                        <th>Course title</th>
                                                        <th>Credit</th>
                                                        <th>Grade</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        year.secSem.map((subject, index) => {
                                                            return (
                                                                <tr>
                                                                    <td>{index + 1}</td>
                                                                    <td>{subject.subjectCode}</td>
                                                                    <td>{subject.subjectName}</td>
                                                                    <td>{subject.credit}</td>
                                                                    <td>{subject.grade}</td>
                                                                </tr>
                                                            )
                                                        })
                                                    }
                                                </tbody>
                                            </Table>

                                            <Table>
                                                <thead>
                                                    <tr>
                                                        <th>credits</th>
                                                        <th>enrolled credits</th>
                                                        <th>GPA</th>
                                                        <th>GPAX</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {
                                                        <tr>
                                                            <td>{year.gpas.secSemRecord.enrolledCredit}</td>
                                                            <td>{year.gpas.secSemRecord.receivedCredit}</td>
                                                            <td>{year.gpas.secSemRecord.gpa}</td>
                                                            <td>{year.gpas.secSemRecord.gpax}</td>
                                                        </tr>
                                                    }
                                                </tbody>
                                            </Table>
                                            <br />
                                            <hr />
                                        </div>
                                        :
                                        <div>
                                            <hr />
                                        </div>
                                }
                            </div>
                        )
                    })
                }
            </div>
            <Footer />
        </div>
    );

}

export default Gpa;