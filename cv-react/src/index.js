import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Route, Redirect} from 'react-router-dom';
import Header from './components/header';
import Gpa from './pages/Gpa';
import aboutMe from './pages/aboutMe';
import Anime from './pages/anime';
import 'antd/dist/antd.css';
import Anime2 from './pages/anime2';

const MainRouting =
    <BrowserRouter>
        <Route path="/" component={Header} />
        <Route path="/aboutMe" component={aboutMe} ></Route>
        <Route path="/Gpa" component={Gpa} exact={true} ></Route>
        <Route path="/anime" component={Anime} exact={true} ></Route>
        <Route path="/anime2" component={Anime2} exact={true}></Route>
        <Route exact path="/" render={()=>{ return <Redirect to="/aboutMe"/>}}/>
    </BrowserRouter>


ReactDOM.render(MainRouting, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
