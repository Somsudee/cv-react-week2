import React from 'react';
import { Form, Icon, Input, Button } from 'antd';
import { connect } from 'react-redux';

const Login = () => {

    const handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };

    return (
        <div className="container" style={{ width: '400px', height: '400px', margin: '10px auto' }}>
            <h1 style={{ textAlign: "center" }}>Login</h1>
            <div>
                <Form onSubmit={handleSubmit} className="login-form">
                    <Form.Item>
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Username"
                            // onChange={handleChangeUser}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Input
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="Password"
                        // onChange={}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button
                            type="danger"
                            htmlType="submit"
                            className="login-form-button"
                            style={{ width: '400px', margin: '0 auto' }}
                            onClick={

                                // hasUserName == true ?
                                () => { window.location = "/processLogin"; }
                                // :
                                // () => alert('wrong username and password')
                            }
                        >
                            Log in
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );

}

export default Login;