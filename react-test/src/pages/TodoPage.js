import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
    setWord, selectedFilter, fetchTodo,
    fetchUser, ChangeToDone
} from '../action/todoAction';
import {
    Descriptions, Row, Col,
    Select, List, Typography,
    Button, PageHeader, Avatar,
    Input
} from 'antd';
import { bindActionCreators } from 'redux';
import Header from '../components/header';
import { useHistory } from 'react-router-dom';

const { Search } = Input;
const { Option } = Select;

const TodoPage = (props) => {

    const { setWord, selectedFilter, fetchTodo, ChangeToDone, fetchUser } = props
    const { user, todoList, targetWord, selected } = props

    const history = useHistory();

    //componetDidMount
    useEffect(() => {
        fetchTodo(props.match.params.user_id);
        fetchUser(props.match.params.user_id);
    }, [])

    const searchWord = ({ target }) => {
        setWord(target.value);
    }

    const handleSelected = (value) => {
        selectedFilter(value);
    }

    return (
        <div>
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                    backgroundColor: '#f0f0f0'
                }}
                onBack={() => history.push("/users")}
                title="Back to UserList"
                extra={[
                    <Header />
                ]}
            />
            <Row type="flex" justify="center">
                <Col span={30}>
                    <Avatar size={64} style={{ backgroundColor: '#f56a00', margin: '20px' }} icon="user" />
                </Col>
                <Col span={30}>
                    <h1 style={{ margin: '35px 1px' }}>{user.name}</h1>
                </Col>
            </Row>
            <hr />
            <Descriptions>
                <Descriptions.Item label="UserName">{user.username}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                <Descriptions.Item label="Address">
                    {
                        user.address != undefined ?
                            <div>{user.address.street}, {user.address.suite}, {user.address.city}  {user.address.zipcode}</div>
                            :
                            <div></div>
                    }
                </Descriptions.Item>
            </Descriptions>
            <div>
                <Search
                    placeholder="Search..."
                    onChange={searchWord}
                    style={{ width: 300, height: 35, margin: '0 10px' }}
                />
                <Select defaultValue="All" style={{ width: 120, marginLeft: 10 }} onChange={handleSelected}>
                    <Option value="All">All</Option>
                    <Option value={false}>Done</Option>
                    <Option value={true}>Doing</Option>
                </Select>

            </div>
            <p></p>

            <List
                header={<h2 style={{ marginBottom: 5 }}>Todo List !!</h2>}
                bordered
                dataSource={
                    selected == 'All' && targetWord == '' ?
                        todoList
                        :
                        selected == 'All' ?
                            todoList.filter(list => list.title.match(targetWord))
                            :
                            todoList.filter(list => list.completed == selected && list.title.match(targetWord))
                }
                renderItem={(item) => (
                    <List.Item>
                        {
                            item.completed ?
                                <div>
                                    <Typography.Text mark> DOING</Typography.Text> {item.title}
                                </div>
                                :
                                <div>
                                    <Typography.Text style={{ textDecoration: 'line-through' }}> DONE </Typography.Text> {item.title}
                                </div>
                        }
                        {
                            item.completed ?
                                <Button type="danger" shape="round" onClick={() => ChangeToDone(item.id - 1)}>Done</Button>
                                :
                                <div></div>
                        }
                    </List.Item>
                )}
            />
        </div>
    )

}


const mapStateToProps = state => {
    return {
        user: state.todo.user,
        todoList: state.todo.todo,
        targetWord: state.todo.targetWord,
        selected: state.todo.selected
    }

}

const mapDisPatchToProps = dispatch => {
    return bindActionCreators({ setWord, selectedFilter, fetchTodo, ChangeToDone, fetchUser }, dispatch)

}

export default connect(mapStateToProps, mapDisPatchToProps)(TodoPage);