import React, { useState, useEffect } from 'react';
import { Descriptions, List, Card, PageHeader, Row, Col, Avatar, Icon } from 'antd';
import Header from '../components/header';
import { useHistory } from 'react-router-dom';

const Photo = (props) => {

    const [user, setUser] = useState({});
    const [album, setAlbum] = useState({});
    const [photo, setPhoto] = useState([]);
    const history = useHistory();

    //componetDidMount
    useEffect(() => {
        fetchUserData();
        fetchAlbumData();
        fetchPhotoData();
    }, [])

    const fetchUserData = () => {
        const userId = props.match.params.album_id;
        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data);
            })
            .catch(error => console.log(error));
    }


    const fetchAlbumData = () => {
        //albums
        const userId = props.match.params.album_id;
        fetch('http://jsonplaceholder.typicode.com/albums?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setAlbum(data);
            })

            .catch(error => console.log(error));
    }

    const fetchPhotoData = () => {
        //Photo
        const albumId = props.match.params.album_id;
        fetch('http://jsonplaceholder.typicode.com/photos?albumId=' + albumId)
            .then(response => response.json())
            .then(data => {
                setPhoto(data);
            })

            .catch(error => console.log(error));
    }

    console.log(album)

    return (
        <div>
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                    backgroundColor: '#f0f0f0'
                }}
                onBack={() => history.push("/users/" + props.match.params.album_id + "/album")}
                title="Back to Albums"
                extra={[
                    <Header />
                ]}
            />
            <Row type="flex" justify="center">
                <Col span={30}>
                    <Avatar size={64} style={{ backgroundColor: '#ffbf00', margin: '20px' }} icon="picture" />
                </Col>
                <Col span={30}>
                    <h1 style={{ margin: '35px 1px' }}>{album[props.match.params.album_id] != undefined ? album[props.match.params.album_id].title : ""}</h1>
                </Col>
            </Row>
            <hr />
            <p></p>
            <Descriptions
                bordered
            >
                <Descriptions.Item label="Name">{user.name}</Descriptions.Item>
                <Descriptions.Item label="UserName">{user.username}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                <Descriptions.Item label="Address">
                    {
                        user.address != undefined ?
                            <div>
                                <div>{user.address.street}, {user.address.suite}, {user.address.city}</div>
                                <div>{user.address.zipcode}</div>
                            </div>
                            :
                            <div></div>
                    }
                </Descriptions.Item>
            </Descriptions>
            <p></p>
            <List
                grid={{ gutter: 16, column: 4 }}
                dataSource={photo}
                renderItem={photo =>
                    <List.Item>
                        <Card title={photo.title} style={{ width: 300 }}
                            cover={
                                <img
                                    alt="example"
                                    src={photo.url}
                                />
                            }
                        />
                    </List.Item>}
            />
        </div>
    )

}

export default Photo;