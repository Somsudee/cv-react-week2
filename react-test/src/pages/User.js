import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { List, Avatar, Input, Skeleton, Row, Popover } from 'antd';
import {
    fetchUser, searchUser
} from '../action/userAction';
import Header from '../components/header';
import { useHistory } from 'react-router-dom';

const { Search } = Input;

const UserPage = (props) => {

    const { users, targetList, searchText } = props
    const { fetchUser, searchUser } = props
    const history = useHistory();

    useEffect(() => {
        fetchUser();
    }, [])

    const handleSearch = (event) => {
        searchUser(event.target.value);
    }

    return (
        <div>
            <Row type='flex' justify='center'>
                <Popover placement="right" content="I'm Home">
                    <Avatar shape="square" size={100} style={{ backgroundColor: '#00a2ae', marginTop: '20px', marginBottom: '5px' }} icon="home" />
                </Popover>
            </Row>
            <Row type='flex' justify='center'>
                <Header style={{ margin: '5px' }} />
            </Row>
            <Row type='flex' justify='center'>
                <Search
                    placeholder="Search..."
                    onChange={handleSearch}
                    style={{ width: 700, height: 50, margin: '10px' }}
                />
            </Row>

            <div className="container">
                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    style={{ margin: '0 100px' }}
                    dataSource={
                        searchText == '' ?
                            users
                            :
                            targetList
                    }
                    renderItem={item => (
                        <List.Item
                            actions={[
                                <a key="list-loadmore-edit" onClick={() => history.push("/users/" + item.id + "/todo")} >Todo</a>,
                                <a key="list-loadmore-more" onClick={() => history.push("/users/" + item.id + "/album")}>Album</a>
                            ]}
                        >
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar size="large" style={{ backgroundColor: '#f56a00' }} icon="user" />
                                    }
                                    title={<a href="https://ant.design">{item.name}</a>}
                                    description={item.email}
                                />
                                <div>{item.phone}</div>
                            </Skeleton>
                        </List.Item>
                    )}
                />

            </div>
        </div>
    );

}


const mapStateToProps = state => {
    return {
        users: state.userCompState.users,
        targetList: state.userCompState.targetList,
        searchText: state.userCompState.searchText
    }

}

const mapDisPatchToProps = dispatch => {
    return bindActionCreators({ fetchUser, searchUser }, dispatch)

}

export default connect(mapStateToProps, mapDisPatchToProps)(UserPage);