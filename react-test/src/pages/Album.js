import React, { useState, useEffect } from 'react';
import { Descriptions, List, Row, Col, Avatar, PageHeader } from 'antd';
import Header from '../components/header';
import { useHistory } from 'react-router';

const Album = (props) => {

    const [user, setUser] = useState({});
    const [album, setAlbum] = useState([]);
    const history = useHistory();

    const fetchUserData = () => {
        const userId = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data);
            })
            .catch(error => console.log(error));
    }

    const fetchAlbumData = () => {
        //albums
        const userId = props.match.params.user_id;
        fetch('http://jsonplaceholder.typicode.com/albums?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setAlbum(data);
            })

            .catch(error => console.log(error));
    }

    //componetDidMount
    useEffect(() => {
        fetchUserData();
        fetchAlbumData();
    }, [])


    return (
        <div>
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                    backgroundColor: '#f0f0f0'
                }}
                onBack={() => history.push("/users")}
                title="Back to UserLists"
                extra={[
                    <Header />
                ]}
            />
            <Row type="flex" justify="center">
                <Col span={30}>
                    <Avatar size={64} style={{ backgroundColor: '#f56a00', margin: '20px' }} icon="user" />
                </Col>
                <Col span={30}>
                    <h1 style={{ margin: '35px 1px' }}>{user.name}</h1>
                </Col>
            </Row>
            <hr />
            <p></p>
            <Descriptions>
                <Descriptions.Item label="UserName">{user.username}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                <Descriptions.Item label="Address">
                    {
                        user.address != undefined ?
                            <div>{user.address.street}, {user.address.suite}, {user.address.city}  {user.address.zipcode}</div>
                            :
                            <div></div>
                    }
                </Descriptions.Item>
            </Descriptions>
            <p></p>
            <List
                size="large"
                header={<h2>Album</h2>}
                footer={<div>footer</div>}
                bordered
                dataSource={album}
                renderItem={album => <List.Item><a onClick={() => history.push("/users/" + album.id + "/photo")} >{album.title}</a></List.Item>}
            />
        </div>
    )

}

export default Album;