import {
    SETLECTED_FILTER, SET_WORD,
    FETCH_TODO_BEGIN, FETCH_TODO_SUCCESS,
    FETCH_TODO_ERROR, CHANGE_TO_DONE,
    FETCH_USER_BEGIN, FETCH_USER_SUCCESS,
    FETCH_USER_ERROR
} from "../action/todoAction";

export const initialTodos = {
    todo: [],
    selected: 'All',
    targetWord: '',
    listTarget: [],
    loading: false,
    error: '',
    user: []
}


export const todoReducer = (state = initialTodos, action) => {
    switch (action.type) {

        case SETLECTED_FILTER:
            state.selected = action.payLoad
            return { ...state }

        case SET_WORD:
            state.targetWord = action.payLoad
            return { ...state }

        case CHANGE_TO_DONE:
            var temp = [...state.todo]
            if (state.user.id == 1) {
                temp[action.index].completed = false;
            } else if ((Math.floor(action.index / 10) % 2) == 0) {
                action.index = action.index - (10 * Math.floor(action.index / 10));
                temp[action.index].completed = false;
            } else {
                action.index = 10 + action.index - (10 * Math.floor(action.index / 10));
                temp[action.index].completed = false;
            }
            return {
                ...state,
                todo: temp
            }
        case FETCH_TODO_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_TODO_SUCCESS:
            return {
                ...state,
                todo: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_TODO_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        case FETCH_USER_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_USER_SUCCESS:
            return {
                ...state,
                user: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_USER_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}