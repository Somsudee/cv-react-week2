import {
    FETCH_USER_BEGIN, SET_IS_LOGIN,
    FETCH_USER_SUCCESS, FETCH_USER_ERROR,
    SET_IS_LOGOUT
} from "../action/loginAction";

export const initialUser = {
    isLogin: false
};

export const loginReducer = (state = initialUser, action) => {
    switch (action.type) {
        case SET_IS_LOGIN:
            return {
                ...state,
                isLogin: true
            }
        case SET_IS_LOGOUT:
            return {
                ...state,
                isLogin: false
            }
        default:
            return state
    }
}