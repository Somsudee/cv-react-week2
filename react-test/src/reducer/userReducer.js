import {
    FETCH_USERS_BEGIN, SEARCH_USER,
    FETCH_USERS_SUCCESS, FETCH_USERS_ERROR
} from "../action/userAction";

export const initialUser = {
    users: [],
    loading: false,
    error: '',
    targetList: [],
    searchText: ''
};

export const userReducer = (state = initialUser, action) => {
    switch (action.type) {
        case SEARCH_USER:
            var targetUser = action.targetName
            var temp = []
            for (var i = 0; i < state.users.length; i++) {
                if (state.users[i].name.indexOf(targetUser) !== -1) {
                    temp.push(state.users[i])
                }
            }
            return {
                ...state,
                targetList: temp,
                searchText: targetUser
            }
        case FETCH_USERS_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_USERS_SUCCESS:
            return {
                ...state,
                users: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_USERS_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}