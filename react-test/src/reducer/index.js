import { combineReducers } from 'redux';
import { todoReducer } from './todoReducer';
import { userReducer } from './userReducer';
import { loginReducer } from './loginReducer';

// export const rootReducer = combineReducers({
//     counterReducer
// })

export const rootReducer = combineReducers({
    todo : todoReducer,
    userCompState : userReducer,
    loginCompState : loginReducer
});