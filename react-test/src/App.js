import React from 'react';
import './App.css';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import UserPage from './pages/User';
import TodoPage from './pages/TodoPage';
import Album from './pages/Album';
import Photo from './pages/Photos';
import Login from './pages/login';
import PrivateRoute from './components/PrivateRouter';
import {
  setIsLogin, setIsLogout
} from './action/loginAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// localStorage.setItem("isLogin", false)

const App = (props) => {

  const { stateLogin, setIsLogout, setIsLogin } = props;

  return (

    <BrowserRouter>
      <Route path="/processLogin" render={() => {
        setIsLogin()
        return <Redirect to="/users" />
      }}
      ></Route>

      <Route path="/processLogout" render={() => {
        setIsLogout()
        return <Redirect to="/login" />
      }}
      ></Route>
      <Route path="/login" component={Login} exact={true} isLogin={stateLogin} />
      <Route path="/" component={Login} exact={true} isLogin={stateLogin} />
      <PrivateRoute path="/users" component={UserPage} exact={true} />
      <PrivateRoute path="/users/:user_id/todo" component={TodoPage} exact={true} />
      <PrivateRoute path="/users/:user_id/album" component={Album} exact={true} /> 
      <PrivateRoute path="/users/:album_id/photo" component={Photo} exact={true} />
    </BrowserRouter>
  );
}



const mapStateToProps = state => {
  return {
    stateLogin: state.loginCompState.isLogin
  }
}

const mapDisPatchToProps = dispatch => {
  return bindActionCreators({ setIsLogout, setIsLogin }, dispatch)
}

export default connect(mapStateToProps, mapDisPatchToProps)(App);
