
//fetch User
export const FETCH_USERS_BEGIN = 'FETCH_USERS_BEGIN';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_ERROR = 'FETCH_USERS_ERROR';

export const SEARCH_USER = 'SEARCH_USER';

export const fetchUser = () => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USERS_BEGIN
    }
}

export const fetchUserSuccess = todos => {
    return {
        type: FETCH_USERS_SUCCESS,
        payLoad: todos
    }
}

export const fetchUserError = error => {
    return {
        type: FETCH_USERS_ERROR,
        payLoad: error
    }
}

export const searchUser = targetName => {
    return {
        type: SEARCH_USER,
        targetName
    }
}