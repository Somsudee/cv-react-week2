
export const ADD_TODO = 'ADD_TODO';
export const SETLECTED_FILTER = 'SETLECTED_FILTER';
export const SET_WORD = 'SET_WORD';
export const CHANGE_TO_DONE = 'CHANGE_TO_DONE';
//fetch todo
export const FETCH_TODO_BEGIN = 'FETCH_TODO_BEGIN';
export const FETCH_TODO_SUCCESS = 'FETCH_TODO_SUCCESS';
export const FETCH_TODO_ERROR = 'FETCH_TODO_ERROR';
//fetch User
export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR';

export const selectedFilter = (selected) => {
    return {
        type: SETLECTED_FILTER,
        payLoad: selected
    }
}

export const setWord = (word) => {
    return {
        type: SET_WORD,
        payLoad: word
    }
}

export const ChangeToDone = (index) => {
    return {
        type: CHANGE_TO_DONE,
        index
    }
}

export const fetchTodo = (userId) => {
    return dispatch => {
        dispatch(fetchTodoBegin())
        return fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchTodoSuccess(data))
            })
            .catch(error => dispatch(fetchTodoError(error)))
    }
}

export const fetchTodoBegin = () => {
    return {
        type: FETCH_TODO_BEGIN
    }
}

export const fetchTodoSuccess = todos => {
    return {
        type: FETCH_TODO_SUCCESS,
        payLoad: todos
    }
}

export const fetchTodoError = error => {
    return {
        type: FETCH_TODO_ERROR,
        payLoad: error
    }
}

export const fetchUser = (userId) => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}


export const fetchUserBegin = () => {
    return {
        type: FETCH_USER_BEGIN
    }
}

export const fetchUserSuccess = todos => {
    return {
        type: FETCH_USER_SUCCESS,
        payLoad: todos
    }
}

export const fetchUserError = error => {
    return {
        type: FETCH_USER_ERROR,
        payLoad: error
    }
}
