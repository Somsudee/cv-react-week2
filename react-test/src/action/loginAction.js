
//fetch User
export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR';

export const SET_IS_LOGIN = 'SET_IS_LOGIN';
export const SET_IS_LOGOUT = 'SET_IS_LOGOUT';

export const fetchUser = () => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USER_BEGIN
    }
}

export const fetchUserSuccess = todos => {
    return {
        type: FETCH_USER_SUCCESS,
        payLoad: todos
    }
}

export const fetchUserError = error => {
    return {
        type: FETCH_USER_ERROR,
        payLoad: error
    }
}

export const setIsLogin = () => {
    return {
        type: SET_IS_LOGIN
    }
}

export const setIsLogout = () => {
    return {
        type: SET_IS_LOGOUT
    }
}