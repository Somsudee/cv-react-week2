import React from 'react';
import { Button, Popover } from 'antd';

const Header = () => {

    return (
        <Popover placement="right" content="行きます~">
            <Button
                type="danger"
                htmlType="submit"
                className="logout-form-button"
                style={{ width:'100px'}}
                onClick={() => {
                    window.location = "/processLogout";
                }}
            >
                Log out
            </Button>
        </Popover>
    );
}
export default Header;