import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ path, stateLogin, component: Component, exact }) => (
    
    <Route path={path} exact={exact} render={(props) => {
        
        if (stateLogin == true)
            return <Component {...props} />
        else
            return <Redirect to="/login" />
    }} />
)


const mapStateToProps = state => {
    return {
      stateLogin: state.loginCompState.isLogin
    }
  }

export default connect(mapStateToProps)(PrivateRoute);